#!/bin/bash
#OAR -n CASP
#OAR -l walltime=48:0:0
#OAR -p gpumem>'16000'
#OAR -O logs/%jobid%.stdout
#OAR -E logs/%jobid%.stderr
source gpu_setVisibleDevices.sh
echo "CUDA_VISIBLE_DEVICES=$CUDA_VISIBLE_DEVICES"
conda activate sgcn-env

MG_LEARNING_PATH=/scratch/prospero/mselosse/s-gcn
source $MG_LEARNING_PATH/scripts/init_linux.sh
cd $MG_LEARNING_PATH 
export PYTHONPATH=$PWD:$PYTHONPATH


echo $MG_METADATA_PATH
echo $MG_TRAIN_DATA 

CASPS=CASP8,CASP9,CASP10,CASP11
python $MG_LEARNING_PATH/src/main/sgcn_train.py \
  --id sgcn_5 \
  --features 3 \
  --network S5AndDropout \
  --conv_nonlinearity elu \
  \
  --train_datasets $CASPS \
  --train_data_path $MG_TRAIN_DATA \
  \
  --atom_types_path $MG_METADATA_PATH/protein_atom_types.txt \
  --include_near_native \
  --normalize_x \
  --normalize_adj \
  --sh_order 5 \
  --threads 4 \
  \
  --optim adam \
  --lr 0.001 \
  --dropout 0.2 \
  --l2_reg 0.003 \
  --loss mse \
  --epochs 100 \
  --train_size 2048 \
  --batch_size 64 \
  --shuffle \
  \
  --checkpoints $MG_CHECKPOINTS_PATH \
  --bad_targets $MG_METADATA_PATH/bad_targets.txt
