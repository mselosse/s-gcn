#!/bin/bash
#SBATCH --job-name casp-sgcn
#SBATCH -A tbr@gpu
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=10
#SBATCH --hint=nomultithread
#SBATCH --time=15:00:00
#SBATCH --output=/gpfswork/rech/tbr/uho58uo/casp-results-node/logs/%j.out
#SBATCH --error=/gpfswork/rech/tbr/uho58uo/casp-results-node/logs/%j.out

module purge 
module load pytorch-gpu/py3/1.6.0


MG_LEARNING_PATH=$WORK/s-gcn
source $MG_LEARNING_PATH/scripts/init_linux.sh
cd $MG_LEARNING_PATH 
export PYTHONPATH=$PWD:$PYTHONPATH


echo $MG_METADATA_PATH
echo $MG_TRAIN_DATA 

CASPS=CASP8,CASP9,CASP10,CASP11
python -u $MG_LEARNING_PATH/src/main/sgcn_train.py \
  --id sgcn_5 \
  --features 3 \
  --network S5AndDropout \
  --conv_nonlinearity elu \
  \
  --train_datasets $CASPS \
  --train_data_path $MG_TRAIN_DATA \
  \
  --atom_types_path $MG_METADATA_PATH/protein_atom_types.txt \
  --include_near_native \
  --normalize_x \
  --normalize_adj \
  --sh_order 5 \
  --threads 4 \
  \
  --optim adam \
  --lr 0.001 \
  --dropout 0.2 \
  --l2_reg 0.003 \
  --loss mse \
  --epochs 100 \
  --train_size 2048 \
  --batch_size 64 \
  --shuffle \
  \
  --checkpoints $MG_CHECKPOINTS_PATH \
  --bad_targets $MG_METADATA_PATH/bad_targets.txt
